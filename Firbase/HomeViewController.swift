//
//  ViewController.swift
//  Firbase
//
//  Created by Pat abhishek on 2021-05-12.
//

import UIKit
import AVKit

class HomeViewController: UIViewController {
    var videoplayer:AVPlayer?
    var videoplayerLayer:AVPlayerLayer?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        //setup video
        let bundlepath=Bundle.main.path(forResource: "myvid", ofType: "mp4")
       
        
        guard bundlepath != nil else {
            return
        }
        let url=URL(fileURLWithPath: bundlepath!)
        let item=AVPlayerItem(url: url)
        videoplayer=AVPlayer(playerItem: item)
        videoplayerLayer=AVPlayerLayer(player: videoplayer!)
        videoplayerLayer?.frame=CGRect(x: -self.view.frame.width*1.5, y: 0, width: self.view.frame.size.width*4, height: self.view.frame.size.height)
        view.layer.insertSublayer(videoplayerLayer!, at: 0)
        videoplayer?.playImmediately(atRate: 0.3)
    }
    


}

