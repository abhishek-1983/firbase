//
//  LoginViewController.swift
//  Firbase
//
//  Created by Pat abhishek on 2021-05-12.
//

import UIKit
import Firebase


class LoginViewController: UIViewController {

    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBAction func btnLogin(_ sender: UIButton) {
        let email=txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let password=txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        //Check if the email and pass word is not nil
        if (email != "" || password != ""){
            self.lblError.text="Email or Password is Empty Please Try Again"
        }
        else{
            //perform Login
            Auth.auth().signIn(withEmail: email!, password: password!) { (Result,Error) in
                if Error != nil{
                    self.lblError.text="Error while Loging in"
                }
                else{
                    //navigate to Main Screen
                    let homevc=(self.storyboard?.instantiateViewController(identifier: "HS"))! as MainViewController
                    self.view.window?.rootViewController=homevc
                    self.view.window?.makeKeyAndVisible()
                }
                
                
        }
        
        
        
        
        
        
        
        
        
       
        }
    }
    @IBOutlet var lblError: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
