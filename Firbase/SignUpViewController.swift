//
//  SignUpViewController.swift
//  Firbase
//
//  Created by Pat abhishek on 2021-05-12.
//

import UIKit
import FirebaseAuth
import  Firebase

class SignUpViewController: UIViewController {

    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBAction func btnSignup(_ sender: UIButton) {
        //check not nil
        
        
        if(txtFirstName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || txtLastName.text?.trimmingCharacters(in: .whitespacesAndNewlines)=="" || txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)=="" || txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)==""){
            lblError.text="Please Fill in all The fields to continue"
        }
        else{
            //create user
            let email=txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            let password=txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            let firstname=txtFirstName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastname=txtLastName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            Auth.auth().createUser(withEmail: email!, password: password!) { (authResult, error) in
                if error != nil{
                    self.lblError.text="Error Creating User"
                }
                else{
                    let db=Firestore.firestore()
                    db.collection("users").addDocument(data: ["firstname":firstname!,"lastname":lastname!,"uid":authResult?.user.uid as Any]) { (error) in
                        if error != nil{
                            self.lblError.text="Error while entering data into Database"
                        }
                        let homevc=(self.storyboard?.instantiateViewController(identifier: "HS"))! as MainViewController
                        self.view.window?.rootViewController=homevc
                        self.view.window?.makeKeyAndVisible()
                    }
                   
                }
            }
        }
        
     
        
        
    }
    @IBOutlet var lblError: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    


}
